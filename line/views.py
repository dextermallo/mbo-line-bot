from django.views.decorators.csrf import csrf_exempt
from django.http.response import HttpResponse
from linebot.exceptions import InvalidSignatureError
from linebot import LineBotApi, WebhookHandler
from django.conf import settings
from linebot.models import (MessageEvent, TextMessage, TextSendMessage, FlexSendMessage,
                            FollowEvent, JoinEvent, PostbackEvent)

from .models import ReplyLoader, LineTextMessage, LinePostbackMessage
from db.controller import (
    create_daily_task, get_today_task, update_today_task, create_monthly_task,
    get_monthly_task, monthly_task_progress_increment
)

line_bot_api = LineBotApi(settings.LINE_CHANNEL_ACCESS_TOKEN)
handler = WebhookHandler(settings.LINE_CHANNEL_SECRET)
reply_loader = ReplyLoader()


@csrf_exempt
def webhook(request):
    signature = request.META['HTTP_X_LINE_SIGNATURE']
    body = request.body.decode()

    try:
        handler.handle(body, signature)
    except InvalidSignatureError:
        return HttpResponse('ok')

    return HttpResponse('ok')


@handler.add(FollowEvent)
def handle_follow(event):
    line_bot_api.reply_message(
        event.reply_token, TextSendMessage(text='Got follow event'))


@handler.add(JoinEvent)
def handle_join(event):
    line_bot_api.reply_message(
        event.reply_token,
        TextSendMessage(text='Joined this ' + event.source.type))


@handler.add(MessageEvent, message=TextMessage)
def handle_message(event):
    msg = LineTextMessage(event)

    if msg.correspond_function == 'daily_goal_tutorial':
        reply_msg = TextSendMessage(reply_loader.data['daily_goals']['tutorial'])
        line_bot_api.reply_message(
            reply_token=msg.reply_token,
            messages=reply_msg)

    elif msg.correspond_function == 'daily_goals_setup':

        resp = create_daily_task(msg.user_id, msg.params)

        if resp['status']:
            reply_msg = TextSendMessage(reply_loader.data['daily_goals']['setup_success'])
            line_bot_api.reply_message(
                reply_token=msg.reply_token,
                messages=reply_msg)
        else:
            reply_msg = TextSendMessage(reply_loader.data['daily_goals']['setup_failed'])
            line_bot_api.reply_message(
                reply_token=msg.reply_token,
                messages=reply_msg)

    elif msg.correspond_function == 'daily_goal_progress':
        resp = get_today_task(msg.user_id)
        if resp['status']:
            reply_msg = FlexSendMessage(
                alt_text="本日目標進度",
                contents=reply_loader.render_daily_task_message(tasks=resp['tasks']))
            line_bot_api.reply_message(msg.reply_token, reply_msg)
        else:
            reply_msg = TextSendMessage(reply_loader.data['daily_goals']['progress_success'])
            line_bot_api.reply_message(
                reply_token=msg.reply_token,
                messages=reply_msg)

    elif msg.correspond_function == 'get_monthly_goals':
        resp = get_monthly_task(user_id=msg.user_id)

        if resp['success']:
            reply_msg = FlexSendMessage(
                alt_text=reply_loader.data['monthly_goals']['contents_alt_text'],
                contents=reply_loader.render_monthly_task_message(resp['date'], resp['tasks'])
            )
        else:
            reply_msg = TextSendMessage(text="資料庫連接有問題！")

        line_bot_api.reply_message(
            reply_token=msg.reply_token,
            messages=reply_msg)

    elif msg.correspond_function == 'create_monthly_task':

        resp = create_monthly_task(msg.user_id, msg.params)

        if resp['status']:
            reply_msg = TextSendMessage(reply_loader.data['monthly_goals']['setup_success'])
            line_bot_api.reply_message(
                reply_token=msg.reply_token,
                messages=reply_msg)
        else:
            reply_msg = TextSendMessage(resp['exception'])
            line_bot_api.reply_message(
                reply_token=msg.reply_token,
                messages=reply_msg)


@handler.add(PostbackEvent)
def handle_postback(event):
    msg = LinePostbackMessage(event)

    if msg.correspond_function == 'update_today_task':
        resp = update_today_task(msg.user_id, msg.data['order'])
        if resp['status']:
            reply_msg = TextSendMessage(ReplyLoader.render_today_finished_task_message(
                task_description=resp['description']
            ))
            line_bot_api.reply_message(
                reply_token=msg.reply_token,
                messages=reply_msg)
        else:
            reply_msg = TextSendMessage(resp['exception'])
            line_bot_api.reply_message(
                reply_token=msg.reply_token,
                messages=reply_msg)
    elif msg.correspond_function == 'monthly_task_progress_increment':
        resp = monthly_task_progress_increment(msg.user_id, msg.data['date'], msg.data['order'])

        if resp['success']:
            resp_monthly_task = get_monthly_task(user_id=msg.user_id)
            if resp_monthly_task['success']:
                reply_msg = FlexSendMessage(
                    alt_text=reply_loader.data['monthly_goals']['contents_alt_text'],
                    contents=reply_loader.render_monthly_task_message(
                        date=resp_monthly_task['date'], 
                        tasks=resp_monthly_task['tasks']
                    )
                )
            else:
                reply_msg = TextSendMessage(text="資料庫連接有問題！")
        else:
            reply_msg = TextSendMessage(text="資料庫連接有問題！")

        line_bot_api.reply_message(
            reply_token=msg.reply_token,
            messages=reply_msg)


def push_message(request):
    msg = [TextSendMessage(text='Hello World!'), TextSendMessage(text='Hello World2!')]

    line_bot_api.push_message('U3d0abe2d9d5ca958b1e60ae1fc141b3e', msg)

    return HttpResponse('ok')
