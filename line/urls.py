from django.urls import path

from line import views


urlpatterns = [

    # webhook
    path("webhook/", views.webhook, name="webhook"),

    # apis
    path('api/message/push/', views.push_message, name="push_message"),

]
