import os
import requests
import json
import abc
import re

from db.helper import get_local_day_str


class LineMessage(metaclass=abc.ABCMeta):
    user_id: str
    reply_token: str
    timestamp: str
    correspond_function: str

    def get_user_display_name(self):

        line_channel_access_token = os.environ['LINE_CHANNEL_ACCESS_TOKEN']

        headers = {'Authorization': 'Bearer ' + line_channel_access_token}
        profile = requests.get(
            f'https://api.line.me/v2/bot/profile/{self.user_id}',
            headers=headers
        )

        self.user_display_name = eval(profile.text)['displayName']

    def to_dict(self):
        pass


class LineTextMessage(LineMessage):

    message: str

    def __init__(self, event):
        self.user_id = event.source.user_id
        self.message = event.message.text
        self.reply_token = event.reply_token
        self.timestamp = event.timestamp
        self.correspond_function = self.identify_respond_function()

    def to_dict(self):
        return {
            'user_id': self.user_id,
            'message': self.message,
            'reply_token': self.reply_token,
            'timestamp': self.timestamp,
            'correspond_function': self.correspond_function,
        }

    def identify_respond_function(self):
        if self.message == '目標設定':
            return 'daily_goal_tutorial'
        elif self.message == '進度完成':
            return 'daily_goal_progress'
        elif self.message == '本月目標':
            return 'get_monthly_goals'
        else:
            msg_first_line = self.message.split('\n')[0]

            # check whether daily goals setup
            daily_goals_setup = re.match('[ ]*(\d{2})[//.]*(\d{2})[ ]*日目標', msg_first_line)
            if daily_goals_setup:
                m = daily_goals_setup.group(1)
                d = daily_goals_setup.group(2)

                self.params = {
                    'month': int(m[1:] if m.startswith('0') else m),
                    'day': int(d[1:] if d.startswith('0') else d),
                    'tasks': self.decompose_daily_tasks()
                }
                return 'daily_goals_setup'

            # check whether monthly goals setup
            create_monthly_goals = re.match('[ ]*(\d{4})[//.]*(\d{2})[ ]*月目標', msg_first_line)
            if create_monthly_goals:
                y = create_monthly_goals.group(1)
                m = create_monthly_goals.group(2)

                self.params = {
                    'year': int(y),
                    'month': int(m[1:] if m.startswith('0') else m),
                    'tasks': self.decompose_monthly_tasks()
                }
                return 'create_monthly_task'

        return None

    def decompose_daily_tasks(self):
        tasks_str = self.message.split('\n')[1:]
        tasks = []

        for task_str in tasks_str:
            task = re.match('(\d+)[\:\. ]*(.*)', task_str)
            order = task.group(1)
            task = task.group(2)
            tasks.append({
                'order': int(order),
                'description': task,
                'status': False,
            })

        return tasks

    def decompose_monthly_tasks(self):
        tasks_str = self.message.split('\n')[1:]
        tasks = []

        for task_str in tasks_str:
            task = re.match('(\d+)[\:\. ]*\((\d+)\)[ ]*(.*)', task_str)
            order = task.group(1)
            required_process_day = task.group(2)
            task = task.group(3)

            tasks.append({
                'order': int(order),
                'description': task,
                'required_process_day': int(required_process_day),
                'finished_day': 0,
            })

        return tasks


class LinePostbackMessage(LineMessage):

    def __init__(self, event):

        self.user_id = event.source.user_id
        self.reply_token = event.reply_token
        self.timestamp = event.timestamp

        data = json.loads(event.postback.data)
        self.correspond_function = data['correspond_function']
        del data['correspond_function']
        self.data = data

    def to_dict(self):
        return {
            'user_id': self.user_id,
            'data': self.data,
            'reply_token': self.reply_token,
            'timestamp': self.timestamp,
            'correspond_function': self.correspond_function,
        }

    def identify_respond_function(self):
        return None


class ReplyLoader:

    data: json

    def __init__(self):
        with open('resources/config/msg.json') as config_file:
            self.data = json.load(config_file)

    def render_daily_task_message(self, tasks):

        flex_msg_contents = self.data['daily_goals']['progress_success']

        # day render
        flex_msg_contents['body']['contents'][0]['contents'][0]['contents'][0]['text'] = get_local_day_str()

        # percentage render
        completion = float(sum([task['status'] for task in tasks if task['status']])/len(tasks)) * 100
        flex_msg_contents['body']['contents'][0]['contents'][1]['text'] = f'{completion}%'
        flex_msg_contents['body']['contents'][1]['width'] = f'{completion}%'

        flex_msg_contents['body']['contents'] = flex_msg_contents['body']['contents'][:2]
        for task in tasks:
            # postback data
            data = {
                'correspond_function': 'update_today_task',
                'order': task['order'],
                'description': task['description'],
            }

            background_color = "#34bf49" if task['status'] else "#ff4c4c"
            task_render_template = {
                "type": "box",
                "layout": "vertical",
                "contents": [
                    {
                        "type": "text",
                        "text": task['description']
                    }
                ],
                "margin": "xl",
                "backgroundColor": background_color,
                "cornerRadius": "4px",
                "paddingAll": "13px",
                "action": {
                    "type": "postback",
                    "label": "label",
                    "data": json.dumps(data),
                    "displayText": f'{task["description"]} 完成🎉！'
                }
            }
            flex_msg_contents['body']['contents'].append(task_render_template)

        return flex_msg_contents

    @staticmethod
    def render_today_finished_task_message(task_description: str):
        return f'🍻恭喜你完成了{task_description}！'

    def render_monthly_task_message(self, date, tasks):
        contents = self.data['monthly_goals']['contents']

        # render date
        contents['body']['contents'][0]['contents'][0]['contents'][0]['text'] = date
        contents['body']['contents'] = contents['body']['contents'][:1]
        for task in tasks:
            # calculate params
            background_color = "#34bf49" if task['finished_day'] == task['required_process_day'] else "#ff4c4c"
            completion = float(task['finished_day'] / task['required_process_day']) * 100

            task_render_template = {
                "type": "box",
                "layout": "horizontal",
                "contents": [
                    {
                        "type": "box",
                        "layout": "vertical",
                        "contents": [
                            {
                                "type": "text",
                                "text": task['description'],
                                "size": "md",
                                "weight": "bold",
                                "color": "#ffffff"
                            },
                            {
                                "type": "text",
                                "text": f"{completion:.2f}% ({task['finished_day']}天 / {task['required_process_day']}天)",
                                "size": "xs",
                                "color": "#ffffff",
                                "margin": "md"
                            },
                            {
                                "type": "box",
                                "layout": "vertical",
                                "contents": [
                                    {
                                        "type": "box",
                                        "layout": "vertical",
                                        "contents": [
                                            {
                                                "type": "filler"
                                            }
                                        ]
                                    }
                                ],
                                "width": f'{completion}%',
                                "backgroundColor": "#ffffff5A",
                                "height": "6px",
                                "margin": "xs"
                            }
                        ],
                        "backgroundColor": background_color,
                        "width": "100%",
                        "cornerRadius": "4px",
                        "paddingAll": "12px"
                    }
                ],
                "cornerRadius": "4px",
                "margin": "xl",
            }

            # postback data
            if task['finished_day'] < task['required_process_day']:
                postback_data = {
                    'correspond_function': 'monthly_task_progress_increment',
                    'order': task['order'],
                    'date': date,
                }
                task_render_template['action'] = {
                  "type": "postback",
                  "data": json.dumps(postback_data),
                  "displayText": f'{task["description"]} 向前邁進了！🎉'
                }

            contents['body']['contents'].append(task_render_template)
        return contents
