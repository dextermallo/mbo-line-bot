from pynamodb.models import Model
from pynamodb.attributes import UnicodeAttribute,\
     ListAttribute, MapAttribute, NumberAttribute, BooleanAttribute
from django.conf import settings


class Task(MapAttribute):
    order = NumberAttribute()
    description = UnicodeAttribute()
    status = BooleanAttribute()

    def to_dict(self):
        return {
            'order': self.order,
            'description': self.description,
            'status': self.status
        }


class DailyTask(Model):
    class Meta:
        table_name = 'daily_task'
        aws_access_key_id = settings.AWS_ACCESS_KEY_ID
        aws_secret_access_key = settings.AWS_SECRET_ACCESS_KEY

    user_id = UnicodeAttribute(hash_key=True)
    date = UnicodeAttribute(range_key=True)
    tasks = ListAttribute(of=Task)


class MonthTask(MapAttribute):
    order = NumberAttribute()
    description = UnicodeAttribute()
    required_process_day = NumberAttribute()
    finished_day = NumberAttribute()

    def to_dict(self):
        return {
            'order': self.order,
            'description': self.description,
            'required_process_day': self.required_process_day,
            'finished_day': self.finished_day
        }


class MonthlyTask(Model):
    class Meta:
        table_name = 'monthly_task'
        aws_access_key_id = settings.AWS_ACCESS_KEY_ID
        aws_secret_access_key = settings.AWS_SECRET_ACCESS_KEY

    user_id = UnicodeAttribute(hash_key=True)
    date = UnicodeAttribute(range_key=True)
    tasks = ListAttribute(of=MonthTask)
