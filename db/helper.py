from datetime import datetime
from dateutil import tz


def get_local_datetime():
    from_zone = tz.tzutc()
    to_zone = tz.tzlocal()

    utc_time = datetime.utcnow().replace(tzinfo=from_zone)
    local_time = utc_time.astimezone(to_zone)

    return local_time


def get_local_day_str():
    return day_to_str(get_local_datetime())


def get_local_month_str():
    return month_to_str(get_local_datetime())


def get_utc_datetime():
    return datetime.utcnow()


def day_to_str(d: datetime):
    return datetime.strftime(d, '%Y-%m-%d')


def month_to_str(d: datetime):
    return datetime.strftime(d, '%Y-%m')
