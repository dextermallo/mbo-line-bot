from db.models import DailyTask, MonthlyTask
from datetime import datetime
from db.helper import get_local_day_str, day_to_str, get_local_month_str, month_to_str


def create_db():
    if not DailyTask.exists():
        DailyTask.create_table(read_capacity_units=5, write_capacity_units=5, wait=True)
    if not MonthlyTask.exists():
        MonthlyTask.create_table(read_capacity_units=5, write_capacity_units=5, wait=True)


def create_daily_task(user_id, params):
    date = day_to_str(datetime.utcnow().replace(month=params['month'], day=params['day']))
    resp = {'status': True}
    try:
        monthly_task = MonthlyTask.get(user_id, date)
        resp['status'] = False
        resp['exception'] = 'The goals of this day already exist.'
        return resp
    except Exception:

        # Item does not exist
        try:
            monthly_task = MonthlyTask(user_id, date)
            monthly_task.tasks = params['tasks']
            monthly_task.save()
        except Exception:
            resp['status'] = False
            resp['exception'] = 'AWS Data access problems.'

    return resp


def get_today_task(user_id):

    now = get_local_day_str()
    resp = {'status': True}
    try:
        daily_task = DailyTask.get(user_id, now)
        resp['tasks'] = [t.to_dict() for t in daily_task.tasks]
    except Exception:
        resp['status'] = False
        resp['exception'] = 'AWS Data access problems.'

    return resp


def update_today_task(user_id, order):

    now = get_local_day_str()
    resp = {'status': True}
    try:
        daily_task = DailyTask.get(user_id, now)
        for idx in range(len(daily_task.tasks)):
            if daily_task.tasks[idx].order == order:
                if not daily_task.tasks[idx].status:
                    daily_task.tasks[idx].status = True
                    daily_task.update(actions=[
                        DailyTask.tasks[idx].set(
                            daily_task.tasks[idx]
                        )
                    ])

                    resp['description'] = daily_task.tasks[idx].description
                    return resp
                else:
                    resp['status'] = False
                    resp['exception'] = 'already Finished.'
    except Exception as e:
        print(e)
        resp['status'] = False
        resp['exception'] = 'AWS Data access problems.'

    return resp


def create_monthly_task(user_id, params):
    create_db()
    date = month_to_str(datetime.utcnow().replace(year=params['year'], month=params['month']))
    resp = {'status': True}
    try:
        monthly_task = MonthlyTask.get(user_id, date)
        resp['status'] = False
        resp['exception'] = 'The goals of this month already exist.'
        return resp
    except Exception:
        # Item does not exist
        try:
            monthly_task = MonthlyTask(user_id, date)
            monthly_task.tasks = params['tasks']
            monthly_task.save()
        except Exception:
            resp['status'] = False
            resp['exception'] = 'AWS Data access problems.'

    return resp


def get_monthly_task(user_id):
    resp = {'success': True}

    try:
        # TODO: fix time
        current_month = get_local_month_str()
        monthly_task = MonthlyTask.get(user_id, current_month)
        resp['date'] = monthly_task.date
        resp['tasks'] = [t.to_dict() for t in monthly_task.tasks]

    except Exception as e:
        resp['success'] = False
        resp['exception'] = e

    return resp


def monthly_task_progress_increment(user_id, date, order):
    resp = {'success': True}

    try:
        monthly_task = MonthlyTask.get(user_id, date)
        task = [t for t in monthly_task.tasks if t.order == order][0]
        task.finished_day += 1
        monthly_task.save()
    except Exception as e:
        resp['success'] = False
        resp['exception'] = e

    return resp
